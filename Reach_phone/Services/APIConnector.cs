﻿using System;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;

namespace Reach_phone
{
	public class APIConnector
	{
		public RestClient restClient;

		#region Delgates
		public delegate void LogHandler(string message);
		public delegate void StatusResponse(bool status, string message);
		public delegate void StatusResponseList<T>(bool status, string message, List<T> nameobject);
		public delegate void StatusResponseObject<T>(bool status, string message, T nameobject);
		public delegate void StatusResponseObjectWithData<T>(bool status, string message, Dictionary<string, string> data, T nameobject);
		#endregion

		static APIConnector _instance;
		static object _lockCreate = new object();
		static TimeSpan connectTimeOut = new TimeSpan(0, 0, 0, 15, 0);
		static string APIToken = "";//NXWWY25De1M_t8yW7zn2
		static string APIUrl = "";

		/// <summary>
		/// Initializes a new instance of the <see cref="T:MasterBeePCL.APIConnector"/> class.
		/// </summary>
		public APIConnector()
		{
			// initialize REST Client
			restClient = new RestClient("https://" + APIUrl);
			restClient.Timeout = connectTimeOut;
		}

		/// <summary>
		/// Gets the instance.
		/// </summary>
		/// <returns>The instance.</returns>
		public static APIConnector GetInstance()
		{
			lock (_lockCreate)
			{
				if (_instance == null)
				{
					if (!String.IsNullOrEmpty(APIUrl))
					{
						_instance = new APIConnector();
					}

				}
			}
			return _instance;
		}

		public static void SetEndpoint(string url)
		{
			APIUrl = url;
		}

		public static string GetEndpoint()
		{
			return APIUrl;
		}

		public void SetAuthToken(string token)
		{
			APIToken = token;
			//restClient.AddDefaultParameter("authToken", token, ParameterType.RequestBody);
		}

		public static string GetAuthToken(string token)
		{
			return APIToken;
		}

		public void DeleteAuthToken()
		{
			APIToken = "";
			//restClient.RemoveDefaultParameter("authToken");
		}

		public async void GetDeltaUpdates(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObject<DeltaResponse> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null);
				}

				var request = new RestRequest("/DeltaCheck", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					var res = JsonConvert.DeserializeObject<DeltaResponse>(response.Content);

					callback(true, "success", res);
				}
				else {
					callback(false, "HTTP error", null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null);
			}
		}

		public async void GetPayload(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObjectWithData<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null, null);
				}

				var request = new RestRequest("/GetPayload", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", data, result);
				}
				else {
					callback(false, "HTTP error", data, null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), data, null);
			}
		}

		public async void SISOToLeave(IConnectivity connectivity, Dictionary<string, string> data, StatusResponse callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error");
				}

				var request = new RestRequest("/SISOToLeave", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success");
				}
				else {
					callback(false, "HTTP error");
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString());
			}
		}

		public async void SaveLeave(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObject<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null);
				}

				var request = new RestRequest("/SaveLeave", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", response.Content);
				}
				else {
					callback(false, "HTTP error", response.Content);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null);
			}
		}

		public async void SaveAttendance(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObject<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null);
				}

				var request = new RestRequest("/SaveAttendance", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", result);
				}
				else {
					callback(false, "HTTP error", null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null);
			}
		}

		public async void SetBoarderLocation(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObject<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null);
				}

				var request = new RestRequest("/SetBoarderLocation", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", result);
				}
				else {
					callback(false, "HTTP error", null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null);
			}
		}

		public async void RecordIncident(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObject<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null);
				}

				var request = new RestRequest("/RecordIncident", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", result);
				}
				else {
					callback(false, "HTTP error", null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null);
			}
		}

		public async void AddIncidentLogEntry(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObjectWithData<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", data, null);
				}

				var request = new RestRequest("/AddIncidentLogEntry", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", data, result);
				}
				else {
					callback(false, "HTTP error", data, null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), data, null);
			}
		}

		public async void GetSinglePastoralReport(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObjectWithData<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null, null);
				}

				var request = new RestRequest("/GetSinglePastoralReport", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", data, result);
				}
				else {
					callback(false, "HTTP error", null, null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null, null);
			}
		}

		public async void BulkSISO(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObject<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null);
				}

				var request = new RestRequest("/BulkSISO", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", result);
				}
				else {
					callback(false, "HTTP error", null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null);
			}
		}

		public async void LoadRes(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObject<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null);
				}

				var request = new RestRequest("/LoadRes", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", result);
				}
				else {
					callback(false, "HTTP error", null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null);
			}
		}

		public async void GetAllUnaddressedPastoralReports(IConnectivity connectivity, Dictionary<string, string> data, StatusResponseObject<string> callback)
		{
			try
			{
				if (!connectivity.IsConnected())
				{
					callback(false, "network error", null);
				}

				var request = new RestRequest("/GetAllUnaddressedPastoralReports", Method.POST);
				request.AddParameter("authToken", APIToken);
				foreach (var param in data)
				{
					request.AddParameter(param.Key, param.Value);
				}

				IRestResponse response = await restClient.Execute(request);

				var result = response.Content;

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//var res = JsonConvert.DeserializeObject<ContactResponse>(response.Content);

					callback(true, "success", result);
				}
				else {
					callback(false, "HTTP error", null);
				}
			}
			catch (Exception ex)
			{
				callback(false, ex.ToString(), null);
			}
		}
	}
}
