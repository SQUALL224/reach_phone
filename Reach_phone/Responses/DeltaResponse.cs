﻿using System;
using System.Collections.Generic;

namespace Reach_phone
{
	public class DeltaResponse
	{
		public List<Delta> d { get; set; }
		public string nid { get; set; }
		public string v { get; set; }
		public string ks { get; set; }
	}

	public class Delta
	{
		public object p;
		public string m;
		public string d;
		public int i;
	}

	public class UpdateLoc
	{
		public string lid { get; set; }
		public string cid { get; set; }
	}

	public class UpdateLeave
	{
		public string cid { get; set; }
		public string reqID { get; set; }
	}

	public class UpdateConfig
	{

	}

	public class UpdateContact
	{
		public string cid { get; set; }
	}

	public class UpdateQuota
	{
	}
}
