// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Reach_phone.iOS
{
    [Register ("HostCell")]
    partial class HostCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lb_boardername { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lb_hostname { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lb_relation { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lb_boardername != null) {
                lb_boardername.Dispose ();
                lb_boardername = null;
            }

            if (lb_hostname != null) {
                lb_hostname.Dispose ();
                lb_hostname = null;
            }

            if (lb_relation != null) {
                lb_relation.Dispose ();
                lb_relation = null;
            }
        }
    }
}