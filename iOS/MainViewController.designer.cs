// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Reach_phone.iOS
{
    [Register ("MainViewController")]
    partial class MainViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_addhost { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_calendar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_myaccount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_myleave { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_newleave { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_notification { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_addhost { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_album { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_calendar { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_myaccount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_myleave { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_newleave { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_notofication { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_reachlogo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_school { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lb_username { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView v_backcolor { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btn_addhost != null) {
                btn_addhost.Dispose ();
                btn_addhost = null;
            }

            if (btn_calendar != null) {
                btn_calendar.Dispose ();
                btn_calendar = null;
            }

            if (btn_myaccount != null) {
                btn_myaccount.Dispose ();
                btn_myaccount = null;
            }

            if (btn_myleave != null) {
                btn_myleave.Dispose ();
                btn_myleave = null;
            }

            if (btn_newleave != null) {
                btn_newleave.Dispose ();
                btn_newleave = null;
            }

            if (btn_notification != null) {
                btn_notification.Dispose ();
                btn_notification = null;
            }

            if (iv_addhost != null) {
                iv_addhost.Dispose ();
                iv_addhost = null;
            }

            if (iv_album != null) {
                iv_album.Dispose ();
                iv_album = null;
            }

            if (iv_calendar != null) {
                iv_calendar.Dispose ();
                iv_calendar = null;
            }

            if (iv_myaccount != null) {
                iv_myaccount.Dispose ();
                iv_myaccount = null;
            }

            if (iv_myleave != null) {
                iv_myleave.Dispose ();
                iv_myleave = null;
            }

            if (iv_newleave != null) {
                iv_newleave.Dispose ();
                iv_newleave = null;
            }

            if (iv_notofication != null) {
                iv_notofication.Dispose ();
                iv_notofication = null;
            }

            if (iv_reachlogo != null) {
                iv_reachlogo.Dispose ();
                iv_reachlogo = null;
            }

            if (iv_school != null) {
                iv_school.Dispose ();
                iv_school = null;
            }

            if (lb_username != null) {
                lb_username.Dispose ();
                lb_username = null;
            }

            if (v_backcolor != null) {
                v_backcolor.Dispose ();
                v_backcolor = null;
            }
        }
    }
}