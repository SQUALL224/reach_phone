using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace Reach_phone.iOS
{
	public class Common
	{
		public string[] months = { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

		public Common()
		{
		}

		public static Loc getLocation_ByLocID(P_P payload, string locID)
		{
			if (payload.fcnf.loc != null)
			{
				for (int i = 0; i < payload.fcnf.loc.Count; i++)
				{
					if (payload.fcnf.loc[i].i == locID)
					{
						return payload.fcnf.loc[i];
					}
				}
			}
			return null;
		}

		public static Lup getLookup_ByItemID(P_P payload, string itemID)
		{
			if (payload.fcnf.lup != null)
			{
				for (int i = 0; i < payload.fcnf.lup.Count; i++)
				{
					if (payload.fcnf.lup[i].i == itemID)
					{
						return payload.fcnf.lup[i];
					}
				}
			}
			return null;
		}

		public List<Docontact> GetContacts_ByPermissionLabel_AndFilterSelection(Payload payload, string permissionLabel, List<string> houseFilters, List<string> yearFilters, List<string> locFilters, List<string> groupFilters)
		{
			List<Docontact> contacts = new List<Docontact>();

			foreach (var contact in payload.d.c)
			{
				if (contact.del == "0" && Security.SpecificContactCan(contact, permissionLabel)
				)
				{
					// Contact matches security profile and is not deleted
					var houseMatch = false;
					var yearMatch = false;
					var locMatch = false;
					var grpMatch = false;

					if (houseFilters.Count > 0)
					{
						foreach (var houseFilter in houseFilters)
						{
							if (houseFilter == contact.h)
							{
								houseMatch = true;
								break;
							}
						}
					}
					else {
						houseMatch = true;
					}

					if (yearFilters.Count > 0)
					{
						foreach (var yearFilter in yearFilters)
						{
							if (yearFilter == contact.y)
							{
								yearMatch = true;
								break;
							}
						}
					}
					else {
						yearMatch = true;
					}

					if (locFilters.Count > 0)
					{
						foreach (var locFilter in locFilters)
						{
							if (locFilter == contact.lid)
							{
								locMatch = true;
								break;
							}
						}
					}
					else {
						locMatch = true;
					}

					if (groupFilters.Count > 0)
					{
						if (contact.grp.Count > 0)
						{
							foreach (var groupFilter in groupFilters)
							{
								foreach (var grp in contact.grp)
								{
									if (groupFilter == grp.i)
									{
										grpMatch = true;
										break;
									}
								}

								if (grpMatch)
								{
									break;
								}
							}
						}
					}
					else {
						grpMatch = true;
					}

					if (houseMatch && yearMatch && locMatch && grpMatch)
					{
						contacts.Add(contact);
					}
				}
			}

			return contacts;
		}

		public static List<object> getContacts_ByTypeIDs(Payload payload, string[] tids)
		{
			List<object> contacts = new List<object>();

			if (payload.cu.tid == "2" || payload.cu.tid == "3" || payload.cu.tid == "4" || payload.cu.tid == "8")
			{
				for (int i = 0; i < payload.d.c.Count; i++)
				{
					for (int j = 0; j < tids.Length; j++)
					{
						if (payload.d.c[i].tid == tids[j])
						{
							contacts.Add(payload.d.c[i]);
							break;
						}
					}
				}
			}
			else if (payload.cu.tid == "5" || payload.cu.tid == "7" || payload.cu.tid == "6")
			{
				for (int j = 0; j < tids.Length; j++)
				{
					if (payload.cu.tid == tids[j])
					{
						contacts.Add(payload.p.dcontact);
					}

					for (int i = 0; i < payload.p.drelcontacts.Count; i++)
					{
						if ((string)payload.p.drelcontacts[i].tid == tids[j])
						{
							contacts.Add(payload.p.drelcontacts[i]);
						}
					}
				}
			}

			return contacts;
		}

		public static string GetContactName(Docontact contact)
		{
			string rst = "";

			if (contact != null)
			{
				var payload = AppManager.getInstance().payloads[0].p;

				switch (payload.fcnf.reach.kvNameLayout)
				{
					case "1":
						rst = contact.l + ", " + contact.f;
						break;
					case "2":
						rst = contact.pn == "" ? contact.l + ", " + contact.f : contact.pn;
						break;
					case "3":
						rst = contact.f + " " + contact.l;
						break;
					case "4":
						rst = contact.l + ", " + contact.f.Substring(0, 1);
						break;
					case "5":
						rst = contact.l + ", " + (contact.pn == "" ? contact.f : contact.pn);
						break;
				}
			}

			return rst;
		}

		public Quota getQuotaCounts_ByContactID_AndQuotaID(Payload payload, string cid, string quotaID)
		{
			try
			{
				foreach (var item in payload.d.q)
				{
					if (item.cid == cid && item.q == quotaID)
					{
						return item;
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("####### getQuotaCounts(): " + ex.ToString());
			}

			return null;
		}

		public static object getContact_ByCID(Payload payload, string cid)
		{
			object contact = null;

			if (payload.cu.tid == "2" || payload.cu.tid == "3" || payload.cu.tid == "4")
			{
				for (int i = 0; i < payload.d.c.Count; i++)
				{
					if (payload.d.c[i].cid == cid)
					{
						contact = payload.d.c[i];
						break;
					}
				}
			}
			else if (payload.cu.tid == "5" || payload.cu.tid == "7" || payload.cu.tid == "6")
			{
				if (payload.cu.cid == cid)
				{
					contact = payload.p.dcontact;
				}
				else
				{
					for (int i = 0; i < payload.p.drelcontacts.Count; i++)
					{
						if (payload.p.drelcontacts[i].cid == cid)
						{
							contact = payload.p.drelcontacts[i];
							break;
						}
					}
				}
			}

			return contact;
		}

		public bool setLocation_ByCID(Payload payload, string cid, string lid)
		{
			try
			{

				if (payload.cu.tid == "2" || payload.cu.tid == "3" || payload.cu.tid == "4")
				{
					payload.d.c.Where(s => s.cid == cid).ToList().ForEach(k =>  k.lid = lid);
				}
				else if (payload.cu.tid == "5" || payload.cu.tid == "7" || payload.cu.tid == "6")
				{
					if (payload.cu.cid == cid)
					{
						payload.p.dcontact.loc = lid;
					}
					else
					{
						//var relContact = payload.p.drelcontacts.Where(s => s.cid == cid).FirstOrDefault();
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				return false;
			}

			return true;
		}

		public ContactType getContactType_ByTID(P_P payload, string tid)
		{
			ContactType type = null;

			for (int i = 0; i < payload.fcnf.ct.Count; i++)
			{
				if ((string)payload.fcnf.ct[i].i == tid)
				{
					type = payload.fcnf.ct[i];
					break;
				}
			}

			return type;
		}
		public Lt getLeaveType_ByTypeID(P_P payload, string tid)
		{
			try
			{
				return payload.fcnf.lt.Where(m => m.i == tid).FirstOrDefault();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				return null;
			}
		}

		public Ltt getLeaveTrans_ByTransID(P_P payload, string transID)
		{
			try
			{
				return payload.fcnf.ltt.Where(m => m.i == transID).FirstOrDefault();
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public static Dcleave GetLeaveReq_ByReqID(P_P payload, string reqID)
		{
			Dcleave req = null;

			for (int i = 0; i < payload.dcleave.Count; i++)
			{
				if ((string)payload.dcleave[i].r == reqID)
				{
					req = payload.dcleave[i];
					break;
				}
			}

			return req;
		}

		public Dcleave getBoarderNextLeaveReqWithinHours(P_P payload, string cid, int hours)
		{
			Dcleave leaveReq = null;

			DateTime now = DateTime.Now.AddMinutes(-30);
			DateTime nowPlusHours = DateTime.Now.AddHours(hours);


			var relative = payload.dcleave.Where(m => m.cid == cid).ToList();
			foreach(var req in relative)
			{
				DateTime ld = Convert.ToDateTime(req.ld);
				DateTime rd = Convert.ToDateTime(req.rd);

				if (req.s == "2" &&
				    req.cid == cid &&
				    req.ald == "0000-00-00 00:00:00" &&
				    DateTime.Compare(rd, now) > 0 &&
				    DateTime.Compare(ld, nowPlusHours) <= 0 &&
				    DateTime.Compare(ld, now) >= 0)
				{
					leaveReq = req;
					break;
				}
			}

			return leaveReq;
		}

		public static Rcc GetRCC_ByItemID(P_P payload, string rid)
		{
			Rcc rst = null;
			try
			{
				var relative = payload.fcnf.rcc.Where(m => m.i == rid).FirstOrDefault();
				return relative;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Common() - GetRCC_ByItemID :" + ex.ToString());
			}

			return rst;
		}

		public List<string> GetRolesThatCan(string permissionLabel)
		{
			List<string> roles = new List<string>();

			//for (var i = 0; i < reach.var.config.rperm.length; i++)
			//{
			//	if (reach.var.config.rperm[i].p == permissionLabel && reach.var.config.rperm[i].v == '1')
			//	{
			//		roles.push(reach.var.config.rperm[i].r);
			//	}
			//}

			return roles;
		}

		public Dcleave getBoarderNextLeaveRequest(P_P payload, string cid)
		{
			Dcleave leaveRequest = null;
			List<Dcleave> leaveReq = new List<Dcleave>();

			DateTime now = DateTime.Now.AddMinutes(-10);

			var tmp = payload.dcleave.Where(m => m.cid == "266").ToList();
			var relatives = payload.dcleave.Where(m => m.s == "2" &&
									  m.cid == cid &&
								  m.ald == "0000-00-00 00:00:00")
				   .ToList();

			                      
			foreach(var leave in relatives)
			{
				try
				{
					DateTime rd = Convert.ToDateTime(leave.rd);

					if (DateTime.Compare(rd, now) >= 0)
					{
						leaveReq.Add(leave);
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("Common - Error on parsing datetime :" + ex.ToString());
				}
			}

			if (leaveReq.Count > 0)
			{
				leaveReq.Sort((x, y) => string.Compare(x.ld, y.ld));
				leaveRequest = leaveReq[0];
			}

			return leaveRequest;
		}

		public static string GetCardinalityTitle(string data)
		{
			if (data == "1")
			{
				return "Positive";
			}
			else if (data == "3")
			{
				return "Negative";
			}
			else
			{
				return "Neutral";
			}
		}

		public static string GetPhoto(Payload payload, Docontact contact, int? size)
		{
			string prefix = "https://" + payload.b;
			if (string.IsNullOrEmpty(contact.ph))
			{
				return prefix + "/images/r3supermantrans.png";
			}
			else {
				if (size == null)
				{
					return prefix + "/__uploads/" + payload.acro + "/photos/" + contact.ph;
				}
				else {
					return prefix + "/__uploads/" + payload.acro + "/photos/_" + size + "_" + contact.ph;
				}
			}
		}
	}
}

