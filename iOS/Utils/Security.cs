using System;
using System.Collections.Generic;
using JavaScriptCore;

namespace Reach_phone.iOS
{
	public class Security
	{

		public static bool DoesUserRolePermission(Docontact contact, string permissionLabel)
		{
			bool rst = false;

			Payload payload = AppManager.getInstance().payloads[0];
			if (payload == null || Configure.Share.ancillaryData.uperm == null || Configure.Share.ancillaryData.rperm == null)
			{
				return false;
			}

			foreach (var perm in Configure.Share.ancillaryData.rperm)
			{
				if (perm.p == permissionLabel)
				{
					if (perm.v == "1" && perm.r == contact.r)
					{
						return true;
					}
				}
			}

			return rst;	
		}

		public static bool UserCan(string permissionLabel)
		{
			bool rst = false;

			Payload payload = AppManager.getInstance().payloads[0];
			if (payload == null || Configure.Share.ancillaryData.uperm == null || Configure.Share.ancillaryData.rperm == null || payload.cu == null)
			{
				return false;
			}

			foreach (var uperm in Configure.Share.ancillaryData.uperm)
			{
				if (uperm.p.Equals(permissionLabel))
				{
					if (uperm.v == "1")
					{
						return true;
					}
					else if (uperm.v == "0")
					{
						return false;
					}
				}
			}

			foreach (var rperm in Configure.Share.ancillaryData.uperm)
			{
				if (rperm.p.Equals(permissionLabel))
				{
					//if (rperm.v == "1" && rperm.r == payload.cu.r)
					//{
					//	return true;
					//}
				}
			}

			return rst;
		}

		public static List<string> GetRolesThatCan(string permissionLabel)
		{
			List<string> roles = new List<string>();

			if (Configure.Share.ancillaryData.rperm == null)
			{
				return roles;
			}

			foreach(var perm in Configure.Share.ancillaryData.rperm)
			{
				if (perm.p == permissionLabel && perm.v == "1")
				{
					roles.Add(perm.r);
				}
			}

			return roles;
		}

		public static List<ContactType> GetContactTypesThatCan(string permissionLabel)
		{
			List<ContactType> ct = new List<ContactType>();
			var roles = GetRolesThatCan(permissionLabel);
			Payload payload = AppManager.getInstance().payloads[0];

			if (roles.Count > 0)
			{
				foreach(var ctype in payload.p.fcnf.ct)
				{
					if (roles.Contains(ctype.r))
					{
						ct.Add(ctype);
					}
				}
			}

			return ct;
		}

		public static bool SpecificContactCan(Docontact contact, string permissionLabel)
		{
			bool rst = false;

			if (Configure.Share.ancillaryData.rperm == null)
			{
				return rst;
			}

			//foreach (var item in contact.up)
			//{
			//	if (contact.up[i].p == permissionLabel)
			//	{
			//		if (contact.up[i].v == '1')
			//		{
			//			return true;
			//		}
			//		else if (contact.up[i].v == '0')
			//		{
			//			return false;
			//		}
			//	}
			//}

			foreach(var item in Configure.Share.ancillaryData.rperm)
			{
				if (item.p == permissionLabel)
				{
					if (item.v == "1" && item.r == contact.r)
					{
						return true;
					} else if (item.v == "1" && item.r == contact.r)
					{
						return false;
					}
				}
			}

			return rst;
		}
	}
}