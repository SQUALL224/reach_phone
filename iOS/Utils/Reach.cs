using System;
using System.Collections.Generic;
using System.Net;
using JavaScriptCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Reach_phone.iOS
{
	public class Configure
	{
		static public string HOST = "";

		static Configure share = null;

		AppManager app;
		JObject _data = null;
		JSContext context = new JSContext();

		#region CoreData
		public CoreData coreData;
		public class CoreData {
			public string acro { get; set; }
			public Fcnf config { get; set; }
			public string sn { get; set; }
			public string sns { get; set; }
			public string version { get; set; }
			/// <summary>
			/// reach.var.config.df
			/// </summary>
			/// <value>The df.</value>
			public string df { get; set; }
			/// <summary>
			/// reach.var.config.isLDAP
			/// </summary>
			/// <value>The is LDAP.</value>
			public string isLDAP { get; set; }
			/// <summary>
			/// reach.var.config.pg
			/// </summary>
			/// <value>The pg.</value>
			public string pg { get; set; }
			/// <summary>
			/// reach.var.config.cache
			/// </summary>
			/// <value>The cache.</value>
			public string cache { get; set; }

			/// <summary>
			/// reach.var.config.roles
			/// </summary>
			/// <value>The roles.</value>
			public string roles { get; set; }

			/// <summary>
			/// reach.var.d.c
			/// </summary>
			/// <value>The c.</value>
			public string c { get; set; }

			/// <summary>
			/// reach.var.d.bloc
			/// </summary>
			/// <value>The roles.</value>
			public string bloc { get; set; }
		}
		#endregion

		#region Ancillary Data
		public AncillaryData ancillaryData;
		public class AncillaryData
		{
			public List<ConfigRperm> rperm { get; set; }
			public List<ConfigUperm> uperm { get; set; }
		}

		public class ConfigRperm
		{
			public string p { get; set; }
			public string v { get; set; }
			public string r { get; set; }
		}
		public class ConfigUperm
		{
			public string p { get; set; }
			public string v { get; set; }
			public string r { get; set; }
		}
		#endregion

		static public Configure Share
		{
			get
			{
				if (share != null) return share;
				share = new Configure();
				return share;
			}
		}

		private Configure()
		{
			app = AppManager.getInstance();
			ancillaryData = new AncillaryData();
			coreData = new CoreData();
		}

		public void Init()
		{
			context.EvaluateScript("reach={}; reach.var = {\n\t\td: {}, \t\t// System data\n\t\ts: [], \t\t// State array\n\t\tconfig: {}, \t// System configuration\n\t\tbhm: [], \n\t\tcu: {\t\t// Current user\n\t\t\tcid: -1\n\t\t}, \n\t\tacro: '', \t// School acronym\n\t\tb: [], \t\t// Boolean state buffer\n\t\tkb: '', \t\t// Keyboard buffer\n\t\tt: null, \t\t// AuthToken cache\n\t\tcdl: false,\t// Core Data Loaded\n\t\tadl: false, \t// Ancillary Data Loaded\n\t\tversion: '',  \t// REACH Current Version\n\t\tcus: {\t\t// REACH Current User Security Matrix\n\t\t\tstaff: false, \n\t\t\tcustodian: false, \n\t\t\tboarder: false, \n\t\t\tkiosk: false\n\t\t}, \n\t\tdebug: true, \n\t\tmmState: false\n\t}");

			int ct = 0;
			TOKEN currentToken = app.authTokens[ct];

			WebClient wc = new WebClient();
			WebClient wc1 = new WebClient();

			wc.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
			wc1.Headers["User-Agent"] = "Mozilla/5.0 (Windows; U; MSIE 9.0; Windows NT 9.0; en-US)";
			//wc1.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

			wc.DownloadStringCompleted += (sender, e) =>
			{
				try
				{
					context.EvaluateScript(e.Result);
					var res = context.EvaluateScript("JSON.stringify(reach)");
					_data = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(res.ToString());
				}
				catch (Exception ex)
				{
				}

				wc1.DownloadStringAsync(new Uri("https://" + HOST + "/LoadRes?c=anc&authToken="+ currentToken.t));
			};
			wc1.DownloadStringCompleted += (sender, e) =>
			{
				try
				{
					context.EvaluateScript(e.Result);

					var res = context.EvaluateScript("JSON.stringify(reach)");
					_data = (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(res.ToString());
				}
				catch (Exception ex)
				{
				}
			};

			wc.DownloadStringAsync(new Uri("https://" + HOST + "/LoadRes?c=core&authToken=" + currentToken.t));
		}

		public JSValue executeJS(string js)
		{
			return context.EvaluateScript(js);
		}
	}
}

