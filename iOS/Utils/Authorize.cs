using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using RestSharp;

namespace Reach_phone.iOS
{
	public class Authorize
	{
		AppManager app;
		LoginViewController loginVC;

		public Authorize(LoginViewController _loginVC)
		{
			app = AppManager.getInstance();
			loginVC = _loginVC;
		}

		public void loginWithAccount(string username, string password)
		{
			RestClient client = new RestClient("https://rsso3.reachboarding.com.au/w61R21vY903HZJx4");

			var request = new RestRequest(Method.POST);
			request.AddParameter("w61R21vY", username);
			request.AddParameter("903HZJx4", password);

			client.ExecuteAsync(request, login_response =>
			{
				Console.WriteLine(login_response.Content);

				try
				{
					var json_login = JsonConvert.DeserializeObject<LoginResponse>(login_response.Content);

					if (json_login.results == null || json_login.results.Count == 0)
					{
						loginVC.showAlert("I'm sorry but there is no Username/Password that matches what you've entered");
					}
					else
					{
						var totalPortalsAuthed = 0;
						var totalPortalsActive = 0;
						var totalPortalFound = json_login.results.Count;

						for (var i = 0; i < json_login.results.Count; i++)
						{
							var portal = json_login.results[i];
							if (portal.a == "1")
							{
								totalPortalsActive++;

								RestClient portal_client = new RestClient("https://" + portal.b + "/Authenticate");

								request = new RestRequest(Method.POST);
								request.AddParameter("username", username);
								request.AddParameter("password", password);

								portal_client.ExecuteAsync(request, token_response =>
								{
									Console.WriteLine(token_response.Content);

									try
									{
										var json_school = JsonConvert.DeserializeObject<SchoolLoginResponse>(token_response.Content);

										if (json_school.tokenID != "-1")
										{
											TOKEN token = new TOKEN();
											token.b = portal.b;
											token.t = json_school.tokenID;

											app.authTokens.Add(token);
											APIConnector.SetEndpoint(token.b);
											APIConnector.GetInstance().SetAuthToken(token.t);
											//push.registerAtREACH(portal.b, json.tokenID); // in order to push notification?
										}

										totalPortalsAuthed++;

										if (totalPortalsAuthed == totalPortalsActive)
										{
											//Configure.HOST = currentToken.b;
											//Configure.Share.Init();

											GetResData();
											processPayloads(username, password);
										}
									}
									catch (Exception e)
									{
										Console.WriteLine(e.ToString());
										loginVC.showAlert("There was something wrong with the data that came back from the server. Please contact Touchline Connect immediately");
									}

								});
							}
						}
					}

				}
				catch (Exception e)
				{
					Console.WriteLine(e.ToString());
					loginVC.showAlert("There was something wrong with the data that came back from the server. Please contact Touchline Connect immediately");
				}
			});
		}

		public void GetResData()
		{
			Dictionary<string, string> data = new Dictionary<string, string>();
			data.Add("c", "core");
			APIConnector.GetInstance().LoadRes(NetworkConnectivity.GetInstance(), data, StatusLoadCore);
		}

		public void StatusLoadCore(bool succ, string msg, string data)
		{
			if (succ)
			{
				try
				{
					app.loadAppCoreData(data);

					Dictionary<string, string> reqData = new Dictionary<string, string>();
					reqData.Add("c", "anc");
					APIConnector.GetInstance().LoadRes(NetworkConnectivity.GetInstance(), reqData, StatusLoadAncillary);
				}
				catch (Exception ex)
				{
					Console.WriteLine("Authorize: StatusLoadCore() --- " + ex.ToString());
				}
			}
		}

		public void StatusLoadAncillary(bool succ, string msg, string data)
		{
			if (succ)
			{
				app.loadAppAncillaryData(data);
			}
		}

		private void processPayloads(string username, string password)
		{
			if (app.authTokens.Count == 0)
			{
				loginVC.showAlert("I'm sorry but there is no Username/Password that matches what you've entered");
			}
			else
			{
				int ct = 0;

				TOKEN currentToken = app.authTokens[ct];

				RestClient client = new RestClient("https://" + currentToken.b + "/AuthPassThrough");

				var request = new RestRequest(Method.GET);
				request.AddParameter("username", username);
				request.AddParameter("password", password);
				request.AddParameter("src", "app");

				client.ExecuteAsync(request, token_response =>
				{
					//Console.WriteLine(token_response.Content);

					try
					{
						var json_payload = JsonConvert.DeserializeObject<PayloadResponse>(token_response.Content);

						string sn = json_payload.p.csn.Replace("<", "").Replace(".", "").Replace("*", "").Replace("?", "").Replace(">", "");
						string acro = json_payload.p.cacro;

						Payload payload = new Payload();
						payload.b = currentToken.b;
						payload.t = currentToken.t;
						payload.p = json_payload.p;
						payload.sn = sn;
						payload.acro = acro;

						Cu cu = new Cu();
						cu.cid = json_payload.p.dcontact.cid;
						cu.tid = json_payload.p.dcontact.tid;
						cu.f = json_payload.p.dcontact.f;
						cu.l = json_payload.p.dcontact.l;

						payload.cu = cu;

						D d = new D();
						payload.d = d;

						app.payloads.Add(payload);

						var tid = json_payload.p.dcontact.tid;

						if (tid == "2" || tid == "3" || tid == "4")
						{
							RestClient contact_client = new RestClient("https://" + currentToken.b + "/GetPayload");

							var contact_request = new RestRequest(Method.POST);
							contact_request.AddParameter("authToken", currentToken.t);
							contact_request.AddParameter("w", "docontacts");

							contact_client.ExecuteAsync(contact_request, contact_response =>
							{
								Console.WriteLine(contact_response.Content);

								try
								{
									var json_contact = JsonConvert.DeserializeObject<ContactResponse>(contact_response.Content);

									Payload p = app.getPayload_ByBaseURL(currentToken.b);

									if (p != null)
									{
										p.d.c = json_contact.p.docontacts;
									}

									ct++;

									finishAppInit();
									//if (ct == app.authTokens.Count)
									//{
									//	finishAppInit();
									//}
									//else {
									//	processPayloads();
									//}

								}
								catch (Exception e)
								{
									Console.WriteLine(e.ToString());
									loginVC.showAlert("There was something wrong with the data that came back from the server. Please try again or contact Touchline Connect immediately");
								}
							});
						}
						else
						{
							ct++;

							if (ct == app.authTokens.Count)
							{
								finishAppInit();
							}
							else
							{
								processPayloads(username, password);
							}
						}
					}
					catch (Exception e)
					{
						Console.WriteLine(e.ToString());
						loginVC.showAlert("There was something wrong with the data that came back from the server. Please try again or contact Touchline Connect immediately");
					}
				});

			}
		}

		private void finishAppInit()
		{
			app.finishAppInit();
		}

	}
}