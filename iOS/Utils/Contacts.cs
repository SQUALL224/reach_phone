using System;
using System.Collections.Generic;
using System.Linq;

namespace Reach_phone.iOS
{
	public class CacheContact
	{
		public string id { get; set; }
		public string text { get; set; }
		public string photo { get; set; }
	}

	public class Contacts
	{
		public string[] assocTypeLabel = { "", "Mother", "Father", "Host", "Child", "Relative", "Guardian", "Loco Parentis", "Other", "Parent", "Emergency Contact"};
		AppManager app;

		public List<CacheContact> cache_staffs;
		public List<CacheContact> cache_boarders;
		public List<CacheContact> cache_mentionboarders;
		public List<BOARDER> boarders;

		static Contacts contacts = null;
		static public Contacts Instance
		{
			get
			{
				if (contacts != null) return contacts;
				contacts = new Contacts();
				return contacts;
			}
		}

		private Contacts()
		{
			app = AppManager.getInstance();
			cache_staffs = new List<CacheContact>();
			cache_boarders = new List<CacheContact>();
			cache_mentionboarders = new List<CacheContact>();
			boarders = new List<BOARDER>();
		}

		public void BuildContactCache()
		{
			Payload payload = app.payloads[0];

			foreach (var contact in payload.d.c)
			{
				if (contact.del == "0")
				{
					if (Security.SpecificContactCan(contact, "BE_STAFF"))
					{
						cache_staffs.Add(new CacheContact { 
							id = contact.cid,
							text = Common.GetContactName(contact),
							photo = Common.GetPhoto(payload, contact, 300)
						});
					}
					else if (Security.SpecificContactCan(contact, "BE_BOARDER"))
					{
						var name = Common.GetContactName(contact);
						var photo = Common.GetPhoto(payload, contact, 300);

						cache_boarders.Add(new CacheContact { 
							id = contact.cid,
							text = name,
							photo = photo
						});

						//contacts.var.cache.mentionBoarders.push({
						//name: name, 
						//username: name, 
						//image: photo
						//});
					}

				}
			}
			cache_boarders = cache_boarders.OrderBy(m => m.text).ToList();
			cache_staffs = cache_staffs.OrderBy(m => m.text).ToList();

			RenderContacts("BE_BOARDER", "", null, null, null, null);
		}

		public Assoc getReverseAssoc(string srcContactID, Docontact contact)
		{
			Assoc assoc = null;

			for (int i=0; i < contact.assoc.Count;i++)
			{
				if (contact.assoc[i].d == srcContactID)
				{
					assoc = contact.assoc[i];
					break;
				}
			}

			return assoc;
		}

		public void RenderContacts(string permission, string searchStr, List<string> houseFilter, List<string> yearFilter, List<string> locFilter, List<string> groupFilter)
		{
			Payload payload = app.payloads[0];

			Dictionary<string, Loc> locCache = new Dictionary<string, Loc>();
			Dictionary<string, Lup> lupCache = new Dictionary<string, Lup>();
			boarders.Clear();

			if (payload != null)
			{
				foreach(var contact in payload.d.c)
				{
					if (contact.del == "0" && !Security.SpecificContactCan(contact, permission))
						continue;

					// Contact matches security profile and is not deleted
					bool houseMatch = true;
					bool yearMatch = true;
					bool locMatch = true;
					bool grpMatch = false;


					if (!string.IsNullOrEmpty(searchStr))
					{
						boarders = boarders.Where(m => m.l.ToLower().Contains(searchStr.ToLower()) || m.f.ToLower().Contains(searchStr.ToLower())).ToList();
					}

					if (houseFilter != null && houseFilter.Count > 0)
					{
						if (houseFilter.Where(m => m == contact.h).FirstOrDefault() == null)
						{
							houseMatch = false;
						}
					}

					if (yearFilter != null && yearFilter.Count > 0)
					{
						if (yearFilter.Where(m => m == contact.y).FirstOrDefault() == null)
						{
							yearMatch = false;
						}
					}

					if (locFilter != null && locFilter.Count > 0)
					{
						if (locFilter.Where(m => m == contact.lid).FirstOrDefault() == null)
						{
							locMatch = false;
						}
					}

					if (groupFilter != null && groupFilter.Count > 0)
					{
						if (contact.grp != null && contact.grp.Count > 0)
						{
							foreach (var groupF in groupFilter)
							{
								foreach (var grp in contact.grp)
								{
									if (groupF == grp.i)
									{
										grpMatch = true;
										break;
									}
								}
								if (grpMatch)
								{
									break;
								}
							}
						}
					}
					else {
						grpMatch = true;
					}

					if (houseMatch && yearMatch && locMatch && grpMatch)
					{
						string locationName = "Unknown Location";
						string locationColour = "#CCC";
						string house = "Unknown House";
						string year = "Unknown Year/Grade";
						string yearIndex = "-1";

						if (!locCache.ContainsKey(contact.lid) || locCache[contact.lid] == null)
						{
							locCache[contact.lid] = Common.getLocation_ByLocID(payload.p, contact.lid);
						}

						if (!lupCache.ContainsKey(contact.h) || lupCache[contact.h] == null)
						{
							lupCache[contact.h] = Common.getLookup_ByItemID(payload.p, contact.h);
						}

						if (!lupCache.ContainsKey(contact.y) || lupCache[contact.y] == null)
						{
							lupCache[contact.y] = Common.getLookup_ByItemID(payload.p, contact.y);
						}

						if (locCache[contact.lid] != null)
						{
							locationName = locCache[contact.lid].l;
							locationColour = locCache[contact.lid].c;
						}

						if (lupCache[contact.h] != null)
						{
							house = lupCache[contact.h].l;
						}

						if (lupCache[contact.y] != null)
						{
							year = lupCache[contact.y].l;
							yearIndex = lupCache[contact.y].i;
						}

						string photo = Common.GetPhoto(payload, contact, 300);

						BOARDER boarder = new BOARDER();
						boarder.contact = contact;
						boarder.cid = contact.cid;
						boarder.name = Util.GetUnescapedString(contact.l + "," + contact.f);
						boarder.l = contact.l;
						boarder.f = contact.f;
						boarder.location = locationName;
						boarder.locationColour = locationColour;
						boarder.house = house;
						boarder.year = year;
						boarder.yearIndex = yearIndex;
						boarder.photo = photo;
						boarder.rid = contact.rid;
						boarder.gat = contact.gat;
						boarder.rm = contact.rm;

						boarders.Add(boarder);
					}
				}
			}

			boarders.Sort((x, y) => string.Compare(x.name, y.name));
		}

	}
}

