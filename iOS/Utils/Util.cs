using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using UIKit;

namespace Reach_phone.iOS
{
	public static class Util
	{
		public static UIColor ColorFromHexString(string hexValue, float alpha = 1.0f)
		{
			var colorString = hexValue.Replace("#", "");
			if (alpha > 1.0f)
			{
				alpha = 1.0f;
			}
			else if (alpha < 0.0f)
			{
				alpha = 0.0f;
			}

			float red, green, blue;

			switch (colorString.Length)
			{
				case 3: // #RGB
					{
						red = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(0, 1)), 16) / 255f;
						green = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(1, 1)), 16) / 255f;
						blue = Convert.ToInt32(string.Format("{0}{0}", colorString.Substring(2, 1)), 16) / 255f;
						return UIColor.FromRGBA(red, green, blue, alpha);
					}
				case 6: // #RRGGBB
					{
						red = Convert.ToInt32(colorString.Substring(0, 2), 16) / 255f;
						green = Convert.ToInt32(colorString.Substring(2, 2), 16) / 255f;
						blue = Convert.ToInt32(colorString.Substring(4, 2), 16) / 255f;
						return UIColor.FromRGBA(red, green, blue, alpha);
					}

				default:
					throw new ArgumentOutOfRangeException(string.Format("Invalid color value {0} is invalid. It should be a hex value of the form #RBG, #RRGGBB", hexValue));
			}
		}

		public static string getCommonStringDate(string str_date)
		{
			//DateTime dt = DateTime.ParseExact(str_date, "MM/dd/yyyy", CultureInfo.InvariantCulture);
			string strFormat = "MM/dd/yyyy";
			try
			{
				DateTime dt = Convert.ToDateTime(str_date, CultureInfo.InvariantCulture);
				string short_date = dt.ToString(strFormat);
				return short_date;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				return strFormat;
			}
		}

		public static string getCommonStringTime(string str_date)
		{
			string strFormat = "hh:mm tt";

			try
			{
				DateTime dt = Convert.ToDateTime(str_date, CultureInfo.InvariantCulture);
				string short_date = dt.ToString(strFormat);
				return short_date;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				return strFormat;
			}

		}

		public static string getShortStringDate(string str_date)
		{
			string strFormat = "yy-MMM-dd ddd";
			try
			{
				DateTime dt = Convert.ToDateTime(str_date, CultureInfo.InvariantCulture);
				string short_date = dt.ToString(strFormat);
				return short_date;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				return strFormat;					
			}
		}

		public static string getSimpleStringDate(string str_date)
		{
			string strFormat = "MM/dd/yyyy";
			try
			{
				DateTime dt = Convert.ToDateTime(str_date, CultureInfo.InvariantCulture);

				string short_date = dt.ToString(strFormat);
				return short_date;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
				return strFormat;
			}

		}

		public static string getStringDateOrTime(bool isDate, string str_date)
		{
			string[] bitz = str_date.Split(' ');
			string[] dbitz = bitz[0].Split('-');
			string[] tbitz = bitz[1].Split(':');

			if (isDate)
			{
				return dbitz[0] + '-' + dbitz[1] + '-' + dbitz[2];
			}
			else {
				return tbitz[0] + ':' + tbitz[1];;
			}
		}

		public static string unsanitise(string clean)
		{
			clean = clean.Replace("::semi::", ";");
			clean = clean.Replace("::amp::", "&");
			clean = clean.Replace("::plus::", ";");

			clean = Regex.Replace(clean, @"<[^>]+>|&nbsp", "").Trim();
			clean = Regex.Replace(clean, @"\s{2,}", "");

			return clean;
		}

		public static string getAcriOfPhrase(string phrase)
		{
			string[] bitz = phrase.Split(' ');
			string acro = "";

			for (int i = 0; i < bitz.Length; i++)
			{
				acro += bitz[i].Substring(0, 1).ToUpper();
			}

			return acro;
		}

		public static string GetUnescapedString(string str)
		{
			try
			{
				return System.Uri.UnescapeDataString(HttpUtility.HtmlDecode(str));
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}

			return str;
		}

		public static string GetEscapedString(string str)
		{
			try
			{
				return System.Uri.EscapeDataString(HttpUtility.HtmlEncode(str));
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}

			return str;
		}

	}
}

