using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;
using Foundation;
using Newtonsoft.Json;
using RestSharp;

namespace Reach_phone.iOS
{
	public class AppManager
	{
		private static AppManager mInstance = null;

		public List<TOKEN> authTokens;
		public List<Payload> payloads;

		public static List<Lup> filters_houses;
		public static List<Lup> filters_years;
		public static List<Loc> filters_locations;
		public static List<Grp> filters_groups;
		public static List<Rcc> filters_categories;
		public static Timer deltaTimer;

		Common common = new Common();

		//public REACH_D d;
		//public REACH_S s;
		//public REACH_CONFIG config;
		//public REACH_CU cu;
		//public string acro;
		//public string uip;uip


		public static bool isBoarder = false, isParent = false, isHost = false, isStaff = false, isKiosk = false;

		bool isProgress = false;
		int deltaItemID = 1;

		public AppManager() {
			authTokens = new List<TOKEN>();
			payloads = new List<Payload>();

			InitDeltaTrigger();
		}

		public static AppManager getInstance()
		{
			if (mInstance == null)
			{
				mInstance = new AppManager();
			}

			return mInstance;
		}

		public void finishAppInit()
		{
			for (var i = 0; i < payloads.Count; i++)
			{
				var p = payloads[i].p;
				string tid = p.dcontact.tid;

				switch (tid)
				{
					case "2":
					case "3":
					case "4":
						isStaff = true;
						break;

					case "5":
						isParent = true;
						break;

					case "6":
						isBoarder = true;
						break;

					case "7":
						isHost = true;
						break;

					case "8":
						isKiosk = true;
						break;
				}
			}

			Contacts.Instance.BuildContactCache();
		}

		public Payload getPayload_ByBaseURL(string baseURL)
		{
			Payload p = null;

			for (var i = 0; i < payloads.Count; i++)
			{
				if ((string)payloads[i].b == baseURL)
				{
					p = payloads[i];
					break;
				}
			}

			return p;
		}

		public bool loadAppCoreData(string CoreContent)
		{
			try
			{
				// reach.var.acro
				string regex_acro = @"reach.var.acro = ([^\n\r]+);";
				string AcroContent = Regex.Match(CoreContent, regex_acro).Groups[1].Value;

				//var acro = JsonConvert.DeserializeObject<List<Configure.ConfigRperm>>(AcroContent);
				Configure.Share.coreData.acro = AcroContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.acro : " + ex.ToString());
			}

			try
			{
				// reach.var.config
				string regex_config = @"reach.var.config = ([^\n\r]+);";
				string ConfigContent = Regex.Match(CoreContent, regex_config).Groups[1].Value;

				Configure.Share.coreData.config = JsonConvert.DeserializeObject<Fcnf>(ConfigContent);
				Payload payload = AppManager.getInstance().payloads[0];

				payload.config = Configure.Share.coreData.config;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config : " + ex.ToString());
			}

			try
			{
				// reach.var.config.sn
				string regex_config_sn = @"reach.var.config.sn = ([^\n\r]+);";
				string ConfigSNContent = Regex.Match(CoreContent, regex_config_sn).Groups[1].Value;

				Configure.Share.coreData.sn = ConfigSNContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config.sn : " + ex.ToString());
			}

			try
			{
				// reach.var.config.sns
				string regex_config_sns = @"reach.var.config.sns = ([^\n\r]+);";
				string ConfigSNSContent = Regex.Match(CoreContent, regex_config_sns).Groups[1].Value;
				Configure.Share.coreData.sns = ConfigSNSContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config.sns : " + ex.ToString());
			}

			try
			{
				// reach.var.config.version
				string regex_config_version = @"reach.var.config.version = ([^\n\r]+);";
				string ConfigVersionContent = Regex.Match(CoreContent, regex_config_version).Groups[1].Value;
				Configure.Share.coreData.version = ConfigVersionContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config.version : " + ex.ToString());
			}


			try
			{
				// reach.var.config.df
				string regex_config_df = @"reach.var.config.df = ([^\n\r]+);";
				string ConfigDFContent = Regex.Match(CoreContent, regex_config_df).Groups[1].Value;
				Configure.Share.coreData.df = ConfigDFContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config.df : " + ex.ToString());
			}

			try
			{
				// reach.var.config.isLDAP
				string regex_config_isldap = @"reach.var.config.isLDAP = ([^\n\r]+);";
				string ConfigIsLDAPContent = Regex.Match(CoreContent, regex_config_isldap).Groups[1].Value;
				Configure.Share.coreData.isLDAP = ConfigIsLDAPContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config.isLDAP : " + ex.ToString());
			}

			try
			{
				// reach.var.config.pg
				string regex_config_pg = @"reach.var.config.pg = ([^\n\r]+);";
				string ConfigPGContent = Regex.Match(CoreContent, regex_config_pg).Groups[1].Value;
				Configure.Share.coreData.pg = ConfigPGContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config.pg : " + ex.ToString());
			}

			try
			{
				// reach.var.config.cache
				string regex_config_cache = @"reach.var.config.cache = ([^\n\r]+);";
				string ConfigCacheContent = Regex.Match(CoreContent, regex_config_cache).Groups[1].Value;
				Configure.Share.coreData.cache = ConfigCacheContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config.cache : " + ex.ToString());
			}

			try
			{
				// reach.var.config.roles
				string regex_config_roles = @"reach.var.config.roles = ([^\n\r]+);";
				string ConfigRolesContent = Regex.Match(CoreContent, regex_config_roles).Groups[1].Value;
				Configure.Share.coreData.roles = ConfigRolesContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.config.roles : " + ex.ToString());
			}

			try
			{
				// reach.var.d.c
				string regex_d_c = @"reach.var.d.c = ([^\n\r]+);";
				string DCContent = Regex.Match(CoreContent, regex_d_c).Groups[1].Value;
				Configure.Share.coreData.c = DCContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.d.c : " + ex.ToString());
			}

			try
			{
				// reach.var.d.bloc
				string regex_d_bloc = @"reach.var.d.bloc = ([^\n\r]+);";
				string DBlocContent = Regex.Match(CoreContent, regex_d_bloc).Groups[1].Value;
				Configure.Share.coreData.bloc = DBlocContent;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing reach.var.d.bloc : " + ex.ToString());
			}

			return true;
		}

		public bool loadAppAncillaryData(string AncContent)
		{
			Payload payload = AppManager.getInstance().payloads[0];

			// reach.var.s
			string regex_s = @"reach.var.s = ([^\n\r]+);";
			string SContent = Regex.Match(AncContent, regex_s).Groups[1].Value;

			try
			{
				// reach.var.config.uperm
				string regex_config_uperm = @"reach.var.config.uperm = ([^\n\r]+);";
				string ConfigUpermContent = Regex.Match(AncContent, regex_config_uperm).Groups[1].Value;

				var uperm = JsonConvert.DeserializeObject<List<Configure.ConfigUperm>>(ConfigUpermContent);
				Configure.Share.ancillaryData.uperm = uperm;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing uPerm : " + ex.ToString());
			}

			try
			{
				// reach.var.config.rperm
				string regex_config_rperm = @"reach.var.config.rperm = ([^\n\r]+);";
				string ConfigRpermContent = Regex.Match(AncContent, regex_config_rperm).Groups[1].Value;

				var rperm = JsonConvert.DeserializeObject<List<Configure.ConfigRperm>>(ConfigRpermContent);
				Configure.Share.ancillaryData.rperm = rperm;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing rPerm : " + ex.ToString());
			}

			try
			{
				// reach.var.d.l
				string regex_d_l = @"reach.var.d.l = ([^\n\r]+);";
				string DLContent = Regex.Match(AncContent, regex_d_l).Groups[1].Value;
				
				var l = JsonConvert.DeserializeObject<List<Dcleave>>(DLContent);
				payload.d.l = l;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing rPerm : " + ex.ToString());
			}

			// reach.var.d.events
			string regex_d_events = @"reach.var.d.events = ([^\n\r]+);";
			string DEventsContent = Regex.Match(AncContent, regex_d_events).Groups[1].Value;

			try
			{
				// reach.var.d.q
				string regex_d_q = @"reach.var.d.q = ([^\n\r]+);";
				string DQContent = Regex.Match(AncContent, regex_d_q).Groups[1].Value;

				var q = JsonConvert.DeserializeObject<List<Quota>>(DQContent);
				payload.d.q = q;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Error parsing rPerm : " + ex.ToString());
			}


			// reach.var.d.ls
			string regex_d_ls = @"reach.var.d.ls = ([^\n\r]+);";
			string DLsContent = Regex.Match(AncContent, regex_d_ls).Groups[1].Value;

			// reach.var.uip
			string regex_uip = @"reach.var.uip = ([^\n\r]+);";
			string UIPContent = Regex.Match(AncContent, regex_uip).Groups[1].Value;

			return true;
		}

		private void InitDeltaTrigger()
		{
			deltaTimer = new Timer();
			deltaTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			deltaTimer.Interval = 30000;

			Console.WriteLine("--- Timer Started ---");
		}

		public void StartDeltaTrigger(bool enabled)
		{
			deltaTimer.Enabled = enabled;
		}

		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			Console.WriteLine("called OnTimedEvent");

			if (!isProgress)
			{
				isProgress = true;

				Dictionary<string, string> data = new Dictionary<string, string>();
				data.Add("i", deltaItemID.ToString());

				APIConnector.GetInstance().GetDeltaUpdates(NetworkConnectivity.GetInstance(), data, StatusDeltaCheck);
			}
		}

		public void StatusDeltaCheck(bool succ, string msg, DeltaResponse json)
		{
			try
			{
				var triggerBLoc = false;

				Dictionary<string, UpdateLeave> leaveObjects = new Dictionary<string, UpdateLeave>();
				List<UpdateConfig> configObjects = new List<UpdateConfig>();
				Dictionary<string, UpdateContact> contactObjects = new Dictionary<string, UpdateContact>();
				List<UpdateLoc> updateLocList = new List<UpdateLoc>();

				if (json.d != null && json.d.Count > 50)
				{
					//"warning", "Whoa! We've got more than 50 items to process here...\r\nWe'll process this in the background for you"
				}

				foreach (var delta in json.d)
				{
					var payload = delta.p;

					switch (delta.m)
					{
						case "loc":
							try
							{
								var locInfo = JsonConvert.DeserializeObject<UpdateLoc>(payload as string);

								var cid = locInfo.cid;
								triggerBLoc = true;

								if (updateLocList.Where(s => s.cid == locInfo.cid).FirstOrDefault() == null)
								{
									updateLocList.Add(locInfo);
								}
								else {
									updateLocList.Where(s => s.cid == locInfo.cid).First().lid = locInfo.lid;
								}
							}
							catch (Exception ex)
							{
								Console.WriteLine("LOC: " + ex.ToString());
							}
							break;

						case "leave":
							try
							{
								var leaveInfo = JsonConvert.DeserializeObject<UpdateLeave>(payload as string);
								var reqID = leaveInfo.reqID;
								if (!leaveObjects.ContainsKey(reqID))
								{
									leaveObjects.Add(reqID, leaveInfo);
								}
							}
							catch (Exception ex)
							{
								Console.WriteLine("Leave: " + ex.ToString());
							}
							break;
						case "config":
							if (configObjects.Count == 0)
							{
								try
								{
									var config = JsonConvert.DeserializeObject<UpdateConfig>(payload as string);
									configObjects.Add(config);
								}
								catch (Exception ex)
								{
									Console.WriteLine("config: " + ex.ToString());
								}
							}
							break;

						case "contact":
							try
							{
								var contactInfo = JsonConvert.DeserializeObject<UpdateContact>(payload as string);
								var contactID = contactInfo.cid;
								if (!contactObjects.ContainsKey(contactID))
								{
									contactObjects.Add(contactID, contactInfo);
								}
							}
							catch (Exception ex)
							{
								Console.WriteLine("contact: " + ex.ToString());
							}
							break;

						case "quota":
							var quotaInfo = JsonConvert.DeserializeObject<Quota>(payload as string);
							ProcessQuotaDelta(quotaInfo);
							break;
					}

					deltaItemID = delta.i;
				}

				foreach (KeyValuePair<string, UpdateLeave> leaveObj in leaveObjects)
				{
					ProcessLeaveDelta(leaveObj);
				}

				foreach (var configObj in configObjects)
				{
					ProcessConfigDelta(configObj);
					break;
				}

				foreach (var contact in contactObjects)
				{
					ProcessContactDelta(contact);
				}

				if (triggerBLoc)
				{
					var cids = updateLocList.Select(i => i.cid.ToString()).Aggregate((s1, s2) => s1 + "," + s2);
					var lids = updateLocList.Select(i => i.lid.ToString()).Aggregate((s1, s2) => s1 + "," + s2);
					NSDictionary notificationInfo = NSDictionary.FromObjectsAndKeys(new NSObject[] { (NSString)cids, (NSString)lids }, new NSObject[] { (NSString)"cids", (NSString)"lids" });

					NSNotificationCenter.DefaultCenter.PostNotificationName("RefreshKiosk", null, notificationInfo);
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
			finally
			{
				isProgress = false;
			}
		}

		public void ProcessQuotaDelta(Quota quota)
		{
			Payload payload = payloads[0];

			var q = common.getQuotaCounts_ByContactID_AndQuotaID(payload, quota.cid, quota.q);

			if (q != null)
			{
				q.c = quota.c;
			}
		}

		public void ProcessConfigDelta(UpdateConfig config)
		{
			Dictionary<string, string> data = new Dictionary<string, string>();
			data.Add("w", "aipcnf");

			APIConnector.GetInstance().GetPayload(NetworkConnectivity.GetInstance(), data, StatusConfigDelta);
		}

		public void ProcessLeaveDelta(KeyValuePair<string, UpdateLeave> leaveInfo)
		{
			Dictionary<string, string> data = new Dictionary<string, string>();
			data.Add("w", "ilr");
			data.Add("r", leaveInfo.Key);

			APIConnector.GetInstance().GetPayload(NetworkConnectivity.GetInstance(), data, StatusLeaveDelta);
		}

		public void ProcessContactDelta(KeyValuePair<string, UpdateContact> contact)
		{
			Dictionary<string, string> data = new Dictionary<string, string>();
			data.Add("w", "ilc");
			data.Add("cid", contact.Key);

			APIConnector.GetInstance().GetPayload(NetworkConnectivity.GetInstance(), data, StatusContactDelta);
		}

		public void StatusContactDelta(bool succ, string msg, Dictionary<string, string> reqdata, string respData)
		{
			Payload payload = payloads[0];

			try
			{
				Docontact contact = (Docontact)Common.getContact_ByCID(payload, reqdata["cid"]);
				var json = JsonConvert.DeserializeObject<DcContactReq>(respData);

				if (contact == null)
				{
					payload.d.c.Add(contact);
				}
				else {
					contact = json.p.c;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		public void StatusConfigDelta(bool succ, string msg, Dictionary<string, string> reqdata, string respData)
		{
			Payload payload = payloads[0];

			try
			{
				//var json = JsonConvert.DeserializeObject<DcLeaveReq>(respData);
				//req = json.p.ilr;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}

		public void StatusLeaveDelta(bool succ, string msg, Dictionary<string, string> reqdata, string respData)
		{
			Payload payload = payloads[0];

			try
			{
				var req = Common.GetLeaveReq_ByReqID(payload.p, reqdata["r"]);
				var json = JsonConvert.DeserializeObject<DcLeaveReq>(respData);

				if (req == null)
				{
					//var existItem = payload.p.dcleave.Where(m => m.r == json.p.ilr.r).FirstOrDefault();
					//if (existItem != null)
					//{
					//}
					//else {
					//}

					payload.p.dcleave.Add(json.p.ilr);
				}
				else {
					req = json.p.ilr;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}
		}
	}
}