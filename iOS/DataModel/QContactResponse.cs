using System;
using System.Collections.Generic;

namespace Reach_phone.iOS
{
	public class QContactResponse
	{
		public QC_P p { get; set; }
	}

	public class QC_P
	{
		public List<Doqcontact> doqcontacts { get; set; }
	}

	public class A1
	{
		public string t { get; set; }
		public string cid { get; set; }
		public string n { get; set; }
	}

	public class Grp1
	{
		public string i { get; set; }
	}

	public class Doqcontact
	{
		public string cid { get; set; }
		public List<A1> a { get; set; }
		public string fb { get; set; }
		public string loc { get; set; }
		public string del { get; set; }
		public string rm { get; set; }
		public string ylup { get; set; }
		public string hlup { get; set; }
		public string ph { get; set; }
		public string rid { get; set; }
		public string tid { get; set; }
		public List<Grp1> grps { get; set; }
		public string m { get; set; }
		public string uid { get; set; }
		public string f { get; set; }
		public string l { get; set; }
		public string gat { get; set; }
		public string e { get; set; }
	}


}

