using System;
namespace Reach_phone.iOS
{
	public class SchoolLoginResponse
	{
		public string code { get; set; }
		public string error { get; set; }
		public string tokenID { get; set; }
	}
}

