using System;
using System.Collections.Generic;

namespace Reach_phone.iOS
{
	public class LoginResponse
	{
		public List<Result> results { get; set;}
	}

	public class Result
	{
		public string a { get; set; }
		public string u { get; set; }
		public string b { get; set; }
	}
}

