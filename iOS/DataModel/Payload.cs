using System;
using System.Collections.Generic;

namespace Reach_phone.iOS
{
	public class Payload
	{
		public string b { get; set; }
		public string t { get; set; }
		public P_P p { get; set; }
		public string sn { get; set; }
		public string acro { get; set; }
		public Cu cu { get; set; }	// Current User
		public D d { get; set; }
		public Fcnf config { get; set; }
	}

	public class Cu
	{
		public string cid { get; set; }
		public string tid { get; set; }
		public string f { get; set; }
		public string l { get; set; }
	}

	public class D
	{
		public List<Docontact> c { get; set; }
		public List<Quota> q { get; set; }
		public List<Dcleave> l { get; set; }
	}

	public class Quota
	{
		public string cid { get; set; }
		public string q { get; set; }
		public string c { get; set; }
	}

	public class DcLeaveReq { 
		public DLP p { get; set; }
	}

	public class DcContactReq
	{
		public DCQ p { get; set; }
	}

	public class DcQuotaReq
	{
		public string ttid { get; set; }
		public string min { get; set; }
		public string l { get; set; }
		public string sd { get; set; }
		public string sdow { get; set; }
		public string ed { get; set; }
		public List<DQC> c { get; set; }
		public DQC_TT tt { get; set; }
		public string max { get; set; }
		public string i { get; set; }
		public DQC_FT ft { get; set; }
	}

	public class DQC_FT
	{
		public string i { get; set; }
		public string l { get; set; }
	}

	public class DQC
	{
		public DQC_P p { get; set; }
		public string l { get; set; }
		public DQC_AT at { get; set; }
		public string atid { get; set; }
	}

	public class DQC_AT
	{
		public string l { get; set; }
	}

	public class DQC_TT
	{
		public string l { get; set; }
	}

	public class DQC_P
	{
		public string m { get; set; }
		public string cids { get; set; }
		public List<string> ctids { get; set; }
	}

	public class DCQ
	{
		public Docontact c { get; set; }
	}

	public class DLP
	{
		public Dcleave ilr { get; set; }
	}

	public class ILR
	{
		public string s { get; set; }
		public string d { get; set; }
		public string rcid { get; set; }
		public string ld { get; set; }
		public string hcid { get; set; }
		public List<ILR_A> a { get; set; }
		public string ard { get; set; }
		public string t { get; set; }
		public string rd { get; set; }
		public string rt { get; set; }
		public string r { get; set; }
		public List<string> si { get; set; }
		public string reqd { get; set; }
		public string ald { get; set; }
		public string lt { get; set; }
		public string n { get; set; }
		public string cid { get; set; }
	}

	public class ILR_A
	{
		public string tid { get; set; }
		public string req { get; set; }
		public string cid { get; set; }
		public string r { get; set; }
		public string ab { get; set; }
		public string s { get; set; }
		public string i { get; set; }
		public string d { get; set; }
	}
}

