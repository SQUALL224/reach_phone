using System;
using System.Collections.Generic;

namespace Reach_phone.iOS
{
	public class ContactResponse
	{
		public C_P p { get; set; }
	}

	public class C_P
	{
		public List<Docontact> docontacts { get; set; }
	}

	public class Med
	{
		public string n { get; set; }
		public string i { get; set; }
	}

	public class Assoc
	{
		public string t { get; set; }
		public string s { get; set; }
		public string d { get; set; }
		public string tl { get; set; }
		public string n { get; set; }
	}

	public class Idt
	{
		public string t { get; set; }
		public string c { get; set; }
		public string tl { get; set; }
	}

	public class Lup1
	{
		public string i { get; set; }
	}

	public class Note
	{
		public string tl { get; set; }
		public string t { get; set; }
		public string c { get; set; }
		public string dt { get; set; }
		public string d { get; set; }
		public string i { get; set; }
	}

	public class Docontact
	{
		public string extpk { get; set; }
		public string lid { get; set; }
		public string un { get; set; }
		public string e { get; set; }
		public string cid { get; set; }
		public List<Med> med { get; set; }
		public string rm { get; set; }
		public string gat { get; set; }
		public string ph { get; set; }
		public string ll { get; set; }
		public string sq { get; set; }
		public string cou { get; set; }
		public string g { get; set; }
		public string dob { get; set; }
		public string su { get; set; }
		public string sa { get; set; }
		public string pc { get; set; }
		public string a1 { get; set; }
		public string a2 { get; set; }
		public string hp { get; set; }
		public string del { get; set; }
		public string tid { get; set; }
		public string url { get; set; }
		public List<Assoc> assoc { get; set; }

		/// <summary>
		/// Gets or sets the firstname of Contact.
		/// </summary>
		/// <value>The f.</value>
		public string f { get; set; }

		/// <summary>
		/// Gets or sets the lastname of contact
		/// </summary>
		/// <value>The l.</value>
		public string l { get; set; }
		public string st { get; set; }

		/// <summary>
		/// Role of contact
		/// </summary>
		/// <value>The r.</value>
		public string r { get; set; }
		public string wp { get; set; }
		public string uid { get; set; }
		public string fb { get; set; }
		public List<Idt> idt { get; set; }
		public string m { get; set; }
		public string h { get; set; }
		public List<Lup1> lups { get; set; }
		public string y { get; set; }
		public List<Grp1> grp { get; set; }
		public List<Note> notes { get; set; }
		public string rid { get; set; }
		public string pac { get; set; }
		public string pin { get; set; }
		public string pn { get; set; }
	}
}

