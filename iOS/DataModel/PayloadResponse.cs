using System;
using System.Collections.Generic;

namespace Reach_phone.iOS
{
	public class PayloadResponse
	{
		public P_P p { get; set; }
	}

	public class P_P
	{
		public List<Cctype> cctypes { get; set; }
		public string cacro { get; set; }
		public Dcontact dcontact { get; set; }
		public List<Dassoc> dassoc { get; set; }
		public Fcnf fcnf { get; set; }
		public string csn { get; set; }
		public List<Dnot> dnot { get; set; }
		public List<Dcleave> dcleave { get; set; }
		public List<Drelcontact> drelcontacts { get; set; }
	}

	public class Cctype
	{
		public string r { get; set; }
		public string i { get; set; }
		public string l { get; set; }
	}

	public class Dassoc
	{
		public string s { get; set; }
		public string d { get; set; }
		public string t { get; set; }
		public string n { get; set; }
	}

	public class Dcontact
	{
		public string sa { get; set; }
		public string a1 { get; set; }
		public List<string> lups { get; set; }
		public string su { get; set; }
		public string w { get; set; }
		public string tid { get; set; }
		public string url { get; set; }
		public string e { get; set; }
		public string ph { get; set; }
		public string loc { get; set; }
		public string p { get; set; }
		public string l { get; set; }
		public string cid { get; set; }
		public string f { get; set; }
		public string sq { get; set; }
		public string rid { get; set; }
		public string hlup { get; set; }
		public string pin { get; set; }
		public string ylup { get; set; }
		public string h { get; set; }
		public string c { get; set; }
		public string m { get; set; }
		public string pn { get; set; }
		public string fb { get; set; }
		public string un { get; set; }
		public string st { get; set; }
		public string rm { get; set; }
		public string a2 { get; set; }
	}

	public class Kitchen
	{
		public string kitchenEmail { get; set; }
	}

	public class Weekly
	{
		public string ehod { get; set; }
		public string sdow { get; set; }
		public string edow { get; set; }
		public string shod { get; set; }
	}

	public class SpecificDate
	{
		public string hod { get; set; }
		public string dow { get; set; }
	}

	public class Pc
	{
		public Weekly weekly { get; set; }
		public string typeID { get; set; }
		public string message { get; set; }
		public string enf { get; set; }
		public SpecificDate specificDate { get; set; }
	}

	public class Rej
	{
		public bool s { get; set; }
		public bool p { get; set; }
		public bool e { get; set; }
	}

	public class App
	{
		public bool s { get; set; }
		public bool p { get; set; }
		public bool e { get; set; }
	}

	public class Auth
	{
		public bool s { get; set; }
		public bool e { get; set; }
		public bool p { get; set; }
	}

	public class Comms
	{
		public Rej rej { get; set; }
		public App app { get; set; }
		public Auth auth { get; set; }
	}

	public class Staff
	{
		public List<string> inc { get; set; }
		public List<string> ex { get; set; }
	}

	public class Pa
	{
		public int escalation { get; set; }
		public int ordinal { get; set; }
		public string typeID { get; set; }
		public Comms comms { get; set; }
		public Staff staff { get; set; }
	}

	public class Pg
	{
		public string active { get; set; }
		public string lateMeal { get; set; }
		public List<string> q { get; set; }
		public string notesReq { get; set; }
		public string locID { get; set; }
	}

	public class Pr
	{
		public List<string> houses { get; set; }
		public List<string> groups { get; set; }
		public List<string> flags { get; set; }
		public List<string> years { get; set; }
	}

	public class Pt
	{
		public List<string> leaving { get; set; }
		public List<string> returning { get; set; }
	}

	public class Lt
	{
		public string lm { get; set; }
		public Pc pc { get; set; }
		public List<Pa> pa { get; set; }
		public string n { get; set; }
		public string l { get; set; }
		public Pg pg { get; set; }
		public string i { get; set; }
		public Pr pr { get; set; }
		public Pt pt { get; set; }
	}

	public class Pastoral
	{
		public string c { get; set; }
		public string i { get; set; }
		public string l { get; set; }
		public string p { get; set; }
	}

	public class ContactType
	{
		public string l { get; set; }
		public string i { get; set; }
		public string r { get; set; }
	}

	public class R
	{
		public List<string> g { get; set; }
		public List<string> f { get; set; }
		public List<string> h { get; set; }
		public List<string> y { get; set; }
	}

	public class Loc
	{
		public string s { get; set; }
		public List<object> co { get; set; }
		public string c { get; set; }
		public List<LL> le { get; set; }
		public string bp { get; set; }
		public string i { get; set; }
		public R r { get; set; }
		public string oc { get; set; }
		public string sp { get; set; }
		public string l { get; set; }
	}

	public class LL
	{
		public string t { get; set; }
		public string r { get; set; }
		public string m { get; set; }
		public string l { get; set; }
		public string y { get; set; }
	}

	public class Cflag
	{
		public string b { get; set; }
		public string i { get; set; }
		public string l { get; set; }
	}

	public class Rcc
	{
		public string l { get; set; }
		public string i { get; set; }
		public string s { get; set; }
	}

	public class Reach
	{
		public string oauthGoogle { get; set; }
		public string headlessKiosk { get; set; }
		public string staffBoarderOverridePIN { get; set; }
		public string schoolName { get; set; }
		public string extUpdateDetails { get; set; }
		public string leaveLocLocation { get; set; }
		public string dateFormat { get; set; }
		public string extUpdateHosts { get; set; }
		public string boarderCanSeeOnCampus { get; set; }
		public string kvNameLayout { get; set; }
		public string campusLocLocation { get; set; }
		public string timeFormat { get; set; }
		public string boarderCanSeeOffCampus { get; set; }
	}

	public class Lup
	{
		public string t { get; set; }
		public string l { get; set; }
		public string tl { get; set; }
		public string i { get; set; }
		public string s { get; set; }
	}

	public class Leave
	{
		public string lateSMSTemplate { get; set; }
		public string showAdHocHost { get; set; }
		public string returnFromLeaveRequiresStaffPIN { get; set; }
		public string allowBoardersToSISOToLeave { get; set; }
		public string leaveRequiresStaffPIN { get; set; }
	}

	public class News
	{
		public string c { get; set; }
		public string h { get; set; }
		public string d { get; set; }
		public string s { get; set; }
		public string i { get; set; }
	}

	public class Grp
	{
		public string tl { get; set; }
		public string i { get; set; }
		public string l { get; set; }
		public string t { get; set; }
	}

	public class EosCat
	{
		public string l { get; set; }
		public string h { get; set; }
		public string o { get; set; }
		public string i { get; set; }
	}

	public class Pt2
	{
		public string i { get; set; }
		public string l { get; set; }
		public List<object> t { get; set; }
	}

	public class L
	{
		public string l { get; set; }
		public string i { get; set; }
	}

	public class Ltt
	{
		public object p { get; set; }
		public string l { get; set; }
		public string i { get; set; }
		public string d { get; set; }
	}

	public class MedCond
	{
		public string i { get; set; }
		public string a { get; set; }
		public string l { get; set; }
	}

	public class Comms2
	{
		public string smsgatewayCallerID { get; set; }
		public string defaultMobileCountryPrefix { get; set; }
		public string emailNotificationFromAddress { get; set; }
	}

	public class Meal
	{
		public string e { get; set; }
		public string l { get; set; }
		public string d { get; set; }
		public string i { get; set; }
		public string s { get; set; }
		public string c { get; set; }
	}

	public class Fcnf
	{
		public Kitchen kitchen { get; set; }
		public List<Lt> lt { get; set; }
		public List<Pastoral> pastoral { get; set; }
		public List<ContactType> ct { get; set; }
		public List<Loc> loc { get; set; }
		public List<Cflag> cflags { get; set; }
		public List<Rcc> rcc { get; set; }
		public Reach reach { get; set; }
		public List<Lup> lup { get; set; }
		public Leave leave { get; set; }
		public List<News> news { get; set; }
		public List<Grp> grp { get; set; }
		public List<EosCat> eosCat { get; set; }
		public List<Pt2> pt { get; set; }
		public List<L> ls { get; set; }
		public List<Ltt> ltt { get; set; }
		public List<MedCond> medCond { get; set; }
		public Comms2 comms { get; set; }
		public List<Meal> meals { get; set; }
		public List<DcQuotaReq> quotas { get; set; }
	}

	public class Dnot
	{
		public string da { get; set; }
		public string s { get; set; }
		public string t { get; set; }
		public string f { get; set; }
		public string r { get; set; }
		public string d { get; set; }
		public string i { get; set; }
		public string p { get; set; }
	}

	public class A
	{
		public string req { get; set; }
		public string tid { get; set; }
		public string s { get; set; }
		public string cid { get; set; }
		public string d { get; set; }
		public string r { get; set; }
		public string i { get; set; }
		public string ab { get; set; }
	}

	public class Dcleave
	{
		public string rtm { get; set; }
		public string rxt { get; set; }
		public string rt { get; set; }
		public string cid { get; set; }
		public string rd { get; set; }
		public string dow { get; set; }
		public string lt { get; set; }
		public List<string> si { get; set; }
		public string n { get; set; }
		public string ald { get; set; }
		public string s { get; set; }
		public string ld { get; set; }
		public int rec { get; set; }
		public string t { get; set; }
		public string ard { get; set; }
		public string r { get; set; }
		public string d { get; set; }
		public string hcid { get; set; }
		public string rud { get; set; }
		public string re { get; set; }
		public string reqd { get; set; }
		public List<A> a { get; set; }
		public string rcid { get; set; }

		// add by t.o
		public string bc { get; set; }
		public string h { get; set; }
		public string src { get; set; }
		public string let { get; set; }
		public string lm { get; set; }
	}

	public class Drelcontact
	{
		public string hlup { get; set; }
		public List<string> lups { get; set; }
		public string m { get; set; }
		public string fb { get; set; }
		public string tid { get; set; }
		public string ylup { get; set; }
		public string l { get; set; }
		public string uid { get; set; }
		public string loc { get; set; }
		public string e { get; set; }
		public string f { get; set; }
		public string cid { get; set; }
	}

	public class ILC {
		public string cid { get; set; }
		public string f { get; set; }
		public string l { get; set; }
		public string tid { get; set; }
		public string a1 { get; set; }
		public string a2 { get; set; }
		public string su { get; set; }
		public string st { get; set; }
		public string pc { get; set; }
		public string cou { get; set; }
		public string hp { get; set; }
		public string wp { get; set; }
		public string m { get; set; }
		public string e { get; set; }
		public string url { get; set; }
		public string dob { get; set; }
		public string uid { get; set; }
		public string lid { get; set; }
		public string del { get; set; }
		public string ph { get; set; }
		public string pin { get; set; }
		public string sq { get; set; }
		public string sa { get; set; }
		public string gat { get; set; }
		public string h { get; set; }
		public string y { get; set; }
		public string fb { get; set; }
		public string pn { get; set; }
		public string g { get; set; }
		public List<ILCAssoc> assoc { get; set; }
		public List<ILCGroup> grp { get; set; }
		public List<ILCIdent> idt { get; set; }
		public List<ILCLookup> lups { get; set; }
		public List<ILCMedical> med { get; set; }
		public List<ILCPerm> up { get; set; }
		public string rid { get; set; }
		public string rm { get; set; }
		public string extpk { get; set; }
		public string[] notes { get; set; }
		public string un { get; set; }
		public string r { get; set; }
		public string cr { get; set; }
	}

	public class ILCAssoc {
		public int s { get; set; }
		public int d { get; set; }
		public int t { get; set; }
		public string tl { get; set; }
		public int n { get; set; }
	}

	public class ILCGroup {
		public int i { get; set; }
	}

	public class ILCIdent {
		public int c { get; set; }
		public int t { get; set; }
		public string tl { get; set; }
	}

	public class ILCLookup {
		public int i { get; set; }
	}

	public class ILCMedical {
		public int i { get; set; }
		public string n { get; set; }
	}

	public class ILCPerm {
		public string p { get; set; }
		public string v { get; set; }
	}
}

