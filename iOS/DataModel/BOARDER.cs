using System;
namespace Reach_phone.iOS
{
	public class BOARDER
	{
		public Docontact contact { get; set; }
		public string cid { get; set; }
		public string name { get; set; }
		public string l { get; set; }
		public string f { get; set; }
		public string location { get; set; }
		public string locationColour { get; set; }
		public string house { get; set; }
		public string year { get; set; }
		public string yearIndex { get; set; }
		public string photo { get; set; }
		public string rid { get; set; }

		/// <summary>
		/// Original meaning is Gated which show stop-icon on student image.
		/// </summary>
		/// <value>The gat.</value>
		public string gat { get; set; }
		public string rm { get; set; }
	}
}

