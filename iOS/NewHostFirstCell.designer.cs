// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Reach_phone.iOS
{
    [Register ("NewHostFirstCell")]
    partial class NewHostFirstCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView iv_boarder { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel ll_name { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (iv_boarder != null) {
                iv_boarder.Dispose ();
                iv_boarder = null;
            }

            if (ll_name != null) {
                ll_name.Dispose ();
                ll_name = null;
            }
        }
    }
}