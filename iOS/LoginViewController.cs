using Foundation;
using System;
using UIKit;
using MBProgressHUD;

namespace Reach_phone.iOS
{
    public partial class LoginViewController : UIViewController
    {
       MTMBProgressHUD hud;

		protected LoginViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			// set Touch Event
			btn_signin.TouchUpInside += Btn_Signin_TouchUpInside;
			btn_forgot.TouchUpInside += Btn_Forgot_TouchUpInside;

			// set Progress View
			hud = new MTMBProgressHUD(View)
			{
				LabelText = "Please wait a moment...",
				RemoveFromSuperViewOnHide = false,
				DimBackground = true
			};
			View.AddSubview(hud);

			vw_login_outer.Layer.CornerRadius = 5.0f;
			vw_login_outer.Layer.MasksToBounds = false;
			//vw_login_outer.BackgroundColor = UIColor.White;
			vw_login_outer.Layer.ShadowColor = Util.ColorFromHexString("fe626d", 1).CGColor;
			vw_login_outer.Layer.ShadowOpacity = 0.8f;
			vw_login_outer.Layer.ShadowRadius = 3;
			vw_login_outer.Layer.ShadowOffset = new CoreGraphics.CGSize(5.0f, 5.0f);

			vw_userid.Layer.BorderWidth = 1;
			vw_userid.Layer.BorderColor = Util.ColorFromHexString("c0979a").CGColor;
			vw_pass.Layer.BorderWidth = 1;
			vw_pass.Layer.BorderColor = Util.ColorFromHexString("c0979a").CGColor;

		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public void showAlert(string message)
		{
			InvokeOnMainThread(delegate
			{
				hud.Hide(true);

				var alert = UIAlertController.Create("", message, UIAlertControllerStyle.Alert);
				alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
				PresentViewController(alert, true, null);
			});
		}

		void Btn_Signin_TouchUpInside(object sender, EventArgs e)
		{
			hud.Show(true);
			tf_username.Text = "REACH123";
			tf_password.Text = "REACH123";


			// Validate Username and Password
			if (tf_username.Text == "")
			{
				showAlert("Please enter your username and try again");
				return;
			}

			if (tf_password.Text == "")
			{
				showAlert("Please enter your password and try again");
				return;
			}

			// Connect to Server and Signin
			InvokeOnMainThread(delegate
			{
				UIStoryboard storyBoard = UIStoryboard.FromName("Main", null);
				MainViewController mainVC = storyBoard.InstantiateViewController("MainViewController") as MainViewController;
				NavigationController.PushViewController(mainVC, true);
			});
			//Authorize auth = new Authorize(this);
			//auth.loginWithAccount(tf_username.Text, tf_password.Text);

			//initSidebar();
		}

		void Btn_Forgot_TouchUpInside(object sender, EventArgs e)
		{
			// Connect to Server and Reset Password
		}


	}
}


