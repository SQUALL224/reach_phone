// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Reach_phone.iOS
{
    [Register ("ManageHostViewController")]
    partial class ManageHostViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_add { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_back { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tbl_host { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btn_add != null) {
                btn_add.Dispose ();
                btn_add = null;
            }

            if (btn_back != null) {
                btn_back.Dispose ();
                btn_back = null;
            }

            if (tbl_host != null) {
                tbl_host.Dispose ();
                tbl_host = null;
            }
        }
    }
}