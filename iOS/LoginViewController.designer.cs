// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Reach_phone.iOS
{
    [Register ("LoginViewController")]
    partial class LoginViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_forgot { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btn_signin { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch swc_remember { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tf_password { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tf_username { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView vw_login_inner { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView vw_login_outer { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView vw_pass { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView vw_userid { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btn_forgot != null) {
                btn_forgot.Dispose ();
                btn_forgot = null;
            }

            if (btn_signin != null) {
                btn_signin.Dispose ();
                btn_signin = null;
            }

            if (swc_remember != null) {
                swc_remember.Dispose ();
                swc_remember = null;
            }

            if (tf_password != null) {
                tf_password.Dispose ();
                tf_password = null;
            }

            if (tf_username != null) {
                tf_username.Dispose ();
                tf_username = null;
            }

            if (vw_login_inner != null) {
                vw_login_inner.Dispose ();
                vw_login_inner = null;
            }

            if (vw_login_outer != null) {
                vw_login_outer.Dispose ();
                vw_login_outer = null;
            }

            if (vw_pass != null) {
                vw_pass.Dispose ();
                vw_pass = null;
            }

            if (vw_userid != null) {
                vw_userid.Dispose ();
                vw_userid = null;
            }
        }
    }
}