// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Reach_phone.iOS
{
    [Register ("NewHostSecondCell")]
    partial class NewHostSecondCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lb_caption { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lb_value { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (lb_caption != null) {
                lb_caption.Dispose ();
                lb_caption = null;
            }

            if (lb_value != null) {
                lb_value.Dispose ();
                lb_value = null;
            }
        }
    }
}